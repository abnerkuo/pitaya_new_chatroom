package services

import (
	"pitaya_new_chatroom/consts"
	"pitaya_new_chatroom/protos"
	"pitaya_new_chatroom/utils"
	"context"
	"encoding/json"
	"fmt"
	"github.com/topfreegames/pitaya"
	"github.com/topfreegames/pitaya/component"
	"github.com/topfreegames/pitaya/logger"
)

type Ops struct{

	component.Base

}

/**
	后端使用 设置服务器状态  运行 还是停止
 */
func (ops *Ops) SetServerStatus(ctx context.Context,data *protos.ServerStatus) ( *protos.RequestRes,error){
	ret:= &protos.ServerStatusUpdateRes{}
	routeName:="setServerStatusRespond"
	ret.Result = false
	server:= pitaya.GetServer()
	if data.Status != consts.Const["SERVICE_STOP"] && data.Status != consts.Const["SERVICE_RUNNING"]{
		ret.Result = false
		res:= utils.Utils.ReturnTextData(routeName,"-1",ctx)
		return res,nil
	}
	server.Metadata["serverStatus"] = data.Status
	ret.Result = true
	res :=utils.Utils.ReturnComplexData(routeName,200 ,ret)

	rpcRet := &protos.RPCRes{}
	rpcReq := &protos.RPCReq{}
	rpcRetData ,_:=  json.Marshal(map[string]string{
		"serverId": pitaya.GetServerID(),
		"status":data.Status,
	})
	rpcReq.Msg = string(rpcRetData)
	//发一个rpc通知所有前端
	err:=pitaya.RPC(ctx,"chatroom.frontendopsremote.serverstatusnotify",rpcRet,rpcReq)

	if err !=nil{
		logger.Log.Error(err.Error())
	}

	fmt.Println(rpcRet)


	return res,nil
}
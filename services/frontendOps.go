package services

import (
"pitaya_new_chatroom/protos"
"pitaya_new_chatroom/utils"
"context"
"encoding/json"
"github.com/topfreegames/pitaya"
"github.com/topfreegames/pitaya/component"
)

type FrontendOps struct{

	component.Base

}

var ServerStatusList = make(map[string]string,0)
/**
给前端使用
返回 []string{ serverID...}
*/
func (ops *FrontendOps) GetServerIds(ctx context.Context,data *protos.ServerTypeReq)(*protos.RequestRes,error){

	serverInfo,_:=pitaya.GetServersByType(data.Type)
	list := &protos.ServerLists{
		List:[]string{},
	}

	if len(serverInfo) >0{
		for id,_:=range serverInfo{
			list.List = append(list.List,id)
		}
	}


	res :=utils.Utils.ReturnComplexData("getServerIdsRespond",200 ,list)

	return res,nil


}

/**
记录一下 后端服务器情况
*/

func (ops *FrontendOps) ServerStatusNotify(ctx context.Context,data *protos.RPCReq) (*protos.RPCRes,error){
	reqData:= make(map[string]string,0)
	json.Unmarshal([]byte(data.Msg),&reqData)
	if backendId ,ok :=reqData["serverId"];ok{
		ServerStatusList[backendId] = reqData["status"]
	}
	rpcRes:= &protos.RPCRes{}
	rpcRes.Code = 200
	rpcRes.Msg = "ok"
	return rpcRes ,nil
}
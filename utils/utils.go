package utils

import (
	"context"
	"encoding/json"
	"fmt"

	"github.com/topfreegames/pitaya"
	"pitaya_new_chatroom/i18n"
	"pitaya_new_chatroom/protos"
	"reflect"
	"strconv"
	"sync"
)

type ErrorTips struct {
	tips map[int32]string
	RW sync.RWMutex

}

func (et *ErrorTips) InitTips() {
	et.tips = map[int32]string{
		10001:"房间不存在",
	}
	return
}



type Pkg struct {

}

var(
	Utils  = &Pkg{}
	ErrorInfo = &ErrorTips{}
	)


func init(){
	
	ErrorInfo.InitTips()
}



/**

根据code 返回提示

*/

func (u *Pkg) ReturnTextData (route string ,code string,ctx context.Context) *protos.RequestRes{

	res:= &protos.RequestRes{}

	text:= pitaya.GetConfig().Get("resCode")

	resText:=""
	if textV ,ok :=text.(map[string]interface{});ok {
		fmt.Println(textV)
		resText = textV[code].(string)
	}


	fmt.Println(resText)
	res.RId = route
	intCode,_:= strconv.Atoi(code)
	info,_ := json.Marshal(map[string]interface{}{
		"code":intCode,
		"data":i18n.I18n.GetText(ctx,resText),
	})
	res.Data = string(info)

	return res
}



/**
自定义 code 和 text 一般用于code是：200
*/
func (u *Pkg) ReturnCustomTextData (route string , code int32,text string,ctx context.Context) *protos.RequestRes{

	res:= &protos.RequestRes{}
	res.RId = route
	info,_ := json.Marshal(map[string]interface{}{
		"code":code,
		"data":i18n.I18n.GetText(ctx,text),
	})
	res.Data = string(info)

	return res
}

func (u *Pkg) ReturnComplexData(route string,code int32,data interface{}) *protos.RequestRes{
	res:= &protos.RequestRes{}
	res.RId = route
	info,_ := json.Marshal(map[string]interface{}{
		"code":code,
		"data":data,
	})
	res.Data = string(info)
	return res
}

//整型 只支持 int64  支持字符串
func (u *Pkg) SliceDeleteToNew(data []interface{},item interface{}) []interface{}{


	newSlice := make([]interface{},0,len(data))

	for _, v:=range data{
		vType :=reflect.TypeOf(v)

		if vType.String() == "string"{
			if v.(string) != item.(string){
				newSlice = append(newSlice,v )
			}
		}else if vType.String() == "int64"{
			if v.(int64) != item.(int64){
				newSlice = append(newSlice,v )
			}
		}

	}

	return newSlice

}
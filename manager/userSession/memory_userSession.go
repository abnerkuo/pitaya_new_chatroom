package userSession

import (
	"sync"
)

var (
	userSessionsMU sync.RWMutex
	userSessions map[string]*SessionInfo
)

func init(){

	userSessions = make(map[string]*SessionInfo)
}



type MemoryUserSession struct {

}


func(c *MemoryUserSession)createSession(nickname string) string{
	return  "session-"+nickname
}

func (c *MemoryUserSession)CreateUserSession(nickname string) (string,error) {
	session:=  c.createSession(nickname)
	return session,nil
}

func (c *MemoryUserSession)StoreUserSession(sessionId string,info *SessionInfo) error {
	userSessionsMU.Lock()
	defer userSessionsMU.Unlock()

	userSessions[sessionId] = info


	return nil
}


func (c *MemoryUserSession)GetUserSession(nickname string) (string,*SessionInfo,error){
		session:=c.createSession(nickname)

		userSessionsMU.Lock()
		defer userSessionsMU.Unlock()
		mg, ok := userSessions[session]

		if!ok{
			return "",nil,nil
		}
		return session,mg,nil
}
package userSession


/**
	sessionId:{
		nickname:xxxxx,
		xxxx:xxxxx

	}
 */

type SessionInfo struct{
	Nickname string
	UID string
	RoomId string
	BackendServerId string
}

type UserSessionService interface {
	CreateUserSession(nickname string) (string, error)

	StoreUserSession(sessionId string, info *SessionInfo) error

	GetUserSession(nickname string) (string, *SessionInfo, error)
}
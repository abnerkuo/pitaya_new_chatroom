package roomManager

import (
	"pitaya_new_chatroom/room"
	"fmt"
	"sync"
)


type RoomManager struct {

	roomList sync.Map
	Mu sync.RWMutex
}

var RoomMgr *RoomManager

func init(){
	RoomMgr = &RoomManager{}
}


// 增加一个房间到 房间管理
func (mgr *RoomManager) AddRoom(roomId string ,room *room.Room) bool{

	mgr.roomList.Store(roomId,room)

	fmt.Println("当前房间情况")

	mgr.roomList.Range(func(id ,room interface{})bool{
		fmt.Printf("当前ID：%s,当前房间信息:%v",id,room)
		return true
	})

	return true

}

// 获取一个房间信息

func (mgr *RoomManager) GetRoom(roomId string) (*room.Room,bool){
	roomInfo,isExists:=mgr.roomList.Load(roomId)

	if !isExists {
		return nil,false
	}

	return roomInfo.(*room.Room),true
}
// 获取所有房间信息

func (mgr *RoomManager) GetRoomAll() sync.Map{


	return mgr.roomList
}

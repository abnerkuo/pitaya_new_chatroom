package i18n

import (
	lang "pitaya_new_chatroom/i18n/languages"
	"context"
	"github.com/topfreegames/pitaya"
	"sync"
)


type Languages struct {

	Lang map[string]map[string]string

	RW sync.RWMutex
}

var I18n *Languages

func init(){

	I18n = &Languages{
		Lang:make(map[string]map[string]string),
	}

	I18n.Lang["ZH"] = lang.ZH

	I18n.Lang["EN"] = lang.EN

}

func (l *Languages) GetText(ctx context.Context,tips string) string{

	s := pitaya.GetSessionFromCtx(ctx)
	userLang :=  s.Get("lang")
	if userLang == nil{
		userLang = "ZH"
		s.Set("lang","ZH")
		s.PushToFront(ctx)
	}

	userLangStr := userLang.(string)

	l.RW.RLock()
	defer l.RW.RUnlock()

	if _,ok := I18n.Lang[userLangStr];!ok {

		userLang = "ZH"
	}

	info:=I18n.Lang[userLangStr]

	if _,ok:=info[tips];!ok{
		return tips
	}


	return info[tips]

}

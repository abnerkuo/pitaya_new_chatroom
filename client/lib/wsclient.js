(function(){
    /**
     * 事件发射器
     * @param obj
     * @returns {Object}
     * @constructor
     */
    function Emitter(obj){
        if(obj){
            return mixin(obj)
        }
    }

    /**
     * Mixin the emitter properties.
     *
     * @param {Object} obj
     * @return {Object}
     * @api private
     */

    function mixin(obj) {
        for (let key in Emitter.prototype) {
            obj[key] = Emitter.prototype[key];
        }
        return obj;
    }


    /**
     * Listen on the given `event` with `fn`.
     * 注册监听一个事件
     * @param {String} event
     * @param {Function} fn
     * @return {Emitter}
     * @api public
     */

    Emitter.prototype.on =
        Emitter.prototype.addEventListener = function(event, fn){
            this._callbacks = this._callbacks || {};
            (this._callbacks[event] = this._callbacks[event] || [])
                .push(fn);
            return this;
        };


    /**
     * Adds an `event` listener that will be invoked a single
     * time then automatically removed.
     * 注册监听一个一次性的事件
     * @param {String} event
     * @param {Function} fn
     * @return {Emitter}
     * @api public
     */

    Emitter.prototype.once = function(event, fn){

        this._callbacks = this._callbacks || {};

        let  on = ()=> {
            this.off(event, on);
            fn.apply(this, arguments);
        }

        on.fn = fn;
        this.on(event, on);
        return this;
    };


    /**
     * Remove the given callback for `event` or all
     * registered callbacks.
     * 删除所有注册事件
     * 删除事件下的所有回调函数
     * 删除事件下的自定回调函数
     * @param {String} event
     * @param {Function} fn
     * @return {Emitter}
     * @api public
     */

    Emitter.prototype.off =
        Emitter.prototype.removeListener =
            Emitter.prototype.removeAllListeners =
                Emitter.prototype.removeEventListener = function(event, fn){
                    this._callbacks = this._callbacks || {};

                    // 删除所有注册的事件
                    if (0 == arguments.length) {
                        this._callbacks = {};
                        return this;
                    }


                    // 删除某一个事件
                    var callbacks = this._callbacks[event];
                    if (!callbacks) return this;

                    //  如果没有传函数 那么删除该事件和事件下所以监听
                    if (1 == arguments.length) {
                        delete this._callbacks[event];
                        return this;
                    }

                    // 删除指定的回调函数
                    let cb;
                    for (let i = 0; i < callbacks.length; i++) {
                        cb = callbacks[i];
                        if (cb === fn || cb.fn === fn) {
                            callbacks.splice(i, 1);
                            break;
                        }
                    }
                    return this;
                };

    /**
     * Emit `event` with the given args.
     * 发射一个事件
     * @param {String} event
     * @param {Mixed} ...
     * @return {Emitter}
     */

    Emitter.prototype.emit = function(event){
        this._callbacks = this._callbacks || {};
        let args = [].slice.call(arguments, 1)
            , callbacks = this._callbacks[event];

        if (callbacks) {
            callbacks = callbacks.slice(0);
            for (let i = 0, len = callbacks.length; i < len; ++i) {
                callbacks[i].apply(this, args);
            }
        }

        return this;
    };


    /**
     * Return array of callbacks for `event`.
     * 返回事件下所有 回调函数
     * @param {String} event
     * @return {Array}
     * @api public
     */

    Emitter.prototype.listeners = function(event){
        this._callbacks = this._callbacks || {};
        return this._callbacks[event] || [];
    };

    /**
     * Check if this emitter has `event` handlers.
     *  检查事件是否有注册
     * @param {String} event
     * @return {Boolean}
     * @api public
     */

    Emitter.prototype.hasListeners = function(event){
        return !! this.listeners(event).length;
    };


    const JS_WS_CLIENT_TYPE = 'js-websocket';
    const JS_WS_CLIENT_VERSION = '0.0.1';

    const RES_OK = 200;
    const RES_FAIL = 500;
    const RES_OLD_CLIENT = 501;

    let Protocol = window.Protocol;
    let decodeIO_encoder = null;
    let decodeIO_decoder = null;
    let Package = Protocol.Package;
    let Message = Protocol.Message;
    let EventEmitter = Emitter;
    let rsa = window.rsa;

    if(typeof(window) != "undefined" && typeof(sys) != 'undefined' && sys.localStorage) {
        window.localStorage = sys.localStorage;
    }
    // 防一下 Object.create 函数不存在
    if (typeof Object.create !== 'function') {
        Object.create = function (o) {
            function F() {}
            F.prototype = o;
            return new F();
        };
    }


    let starx = Object.create(EventEmitter.prototype); // object extend from object


    let socket = null;
    let reqId = 0;
    let callbacks = {};
    let handlers = {};
    //Map from request id to route
    let routeMap = {};
    let dict = {};    // route string to code
    let abbrs = {};   // code to route string

    let heartbeatInterval = 0;
    let heartbeatTimeout = 0;
    let nextHeartbeatTimeout = 0;
    let gapThreshold = 100;   // heartbeat gap threashold
    let heartbeatId = null;
    let heartbeatTimeoutId = null;
    let handshakeCallback = null;

    let decode = null;
    let encode = null;

    let reconnect = false;
    let reconncetTimer = null;
    let reconnectUrl = null;
    let reconnectAttempts = 0;
    let reconnectionDelay = 5000;
    let DEFAULT_MAX_RECONNECT_ATTEMPTS = 10;
    let serializer =  ""

    let handler = {};

    // 是否使用了tls wss ？
    let useCrypto;

    // 握手信息包体结构
    let handshakeBuffer = {
        'sys': {
            type: JS_WS_CLIENT_TYPE,
            version: JS_WS_CLIENT_VERSION,
            rsa: {}
        },
        'user': {
        }
    };

    let initCallback = null;

    // 准备连接webscoket服务器
    starx.init = function(params, cb) {
        initCallback = cb;
        var host = params.host;
        var port = params.port;
        var path = params.path;

        encode = params.encode || defaultEncode;
        decode = params.decode || defaultDecode;

        var url = 'ws://' + host;
        if(port) {
            url +=  ':' + port;
        }

        if(path) {
            url += path;
        }

        handshakeBuffer.user = params.user;
        //todo 加密后面看看
        if(params.encrypt) {
            useCrypto = true;
            rsa.generate(1024, "10001");
            var data = {
                rsa_n: rsa.n.toString(16),
                rsa_e: rsa.e
            };
            handshakeBuffer.sys.rsa = data;
        }

        handshakeCallback = params.handshakeCallback;

        connect(params, url, cb);
    };

    // 默认消息解包函数
    var defaultDecode = starx.decode = function(data) {
        var msg = Message.decode(data);
        if(msg.id > 0){
            msg.route = routeMap[msg.id];
            delete routeMap[msg.id];
            if(!msg.route){
                return;
            }
        }

        msg.body = deCompose(msg);
        return msg;
    };


    let defaultEncode = starx.encode = function(reqId, route, msg) {

        let type = reqId ? Message.TYPE_REQUEST : Message.TYPE_NOTIFY;
        //封包 string->[]byte utf-8
        if(decodeIO_encoder && decodeIO_encoder.lookup(route)) {
            var Builder = decodeIO_encoder.build(route);
            msg = new Builder(msg).encodeNB();
        } else {

            if(serializer === "protobuf" ){

                msg = cc.newEncode(route,msg)
            }else {

                msg = Package.strEncode(JSON.stringify(msg));
            }
        }

        // 是否设置了 压缩路由  压缩路由传给服务器的是一个number
        let compressRoute = 0;
        if(dict && dict[route]) {
            route = dict[route];
            compressRoute = 1;
        }
        return Message.encode(reqId, type, compressRoute, route, msg);
    };


    //请求连接websocket服务器
    let connect = function(params, url, cb) {
        console.log('connect to ' + url);
        params = params || {};
        let maxReconnectAttempts = params.maxReconnectAttempts || DEFAULT_MAX_RECONNECT_ATTEMPTS;
        reconnectUrl = url;

        let onopen = function(event) {

            if(!!reconnect) {
                starx.emit('reconnect');
            }
            reset();

            let d = Package.strEncode(JSON.stringify(handshakeBuffer));

            let obj = Package.encode(Package.TYPE_HANDSHAKE, d);

            send(obj);
        };
        let onmessage = function(event) {

            let resData = Package.decode(event.data);
            processPackage(resData);
            // new package arrived, update the heartbeat timeout
            if(heartbeatTimeout) {
                nextHeartbeatTimeout = Date.now() + heartbeatTimeout;
            }
        };
        let onerror = function(event) {
            starx.emit('io-error', event);
            console.error('socket error: ', event);
        };
        let onclose = function(event) {
            starx.emit('close',event);
            starx.emit('disconnect', event);
            console.log('socket close: ', event);
            if(!!params.reconnect && reconnectAttempts < maxReconnectAttempts) {
                reconnect = true;
                reconnectAttempts++;
                reconncetTimer = setTimeout(function() {
                    cc.log("重连中")
                    connect(params, reconnectUrl, cb);
                }, reconnectionDelay);
                //每重连一次 2的幂次方 延迟重连间隔
                reconnectionDelay *= 2;
            }
        };
        socket = new WebSocket(url);
        // 设置二进制类型为 arraybuffer （一般二进制消息 都是用 Uint8Array）
        socket.binaryType = 'arraybuffer';
        socket.onopen = onopen;
        socket.onmessage = onmessage;
        socket.onerror = onerror;
        socket.onclose = onclose;
    };

    //发送请求消息
    starx.request = function(route, msg, cb) {
        if(arguments.length === 2 && typeof msg === 'function') {
            cb = msg;
            msg = {};
        } else {
            msg = msg || {};
        }
        route = route || msg.route;
        if(!route) {
            return;
        }

        reqId++;
        sendMessage(reqId, route, msg);

        callbacks[reqId] = cb;
        routeMap[reqId] = route;
    };

    //发送通知消息
    starx.notify = function(route, msg) {
        msg = msg || {};
        sendMessage(0, route, msg);
    };

    //发送消息
    let sendMessage = function(reqId, route, msg) {

        if(useCrypto) {
            msg = JSON.stringify(msg);
            let sig = rsa.signString(msg, "sha256");
            msg = JSON.parse(msg);
            msg['__crypto__'] = sig;
        }

        if(encode) {
            msg = encode(reqId, route, msg);
        }

        let packet = Package.encode(Package.TYPE_DATA, msg);

        send(packet);
    };

    // 握手消息 回调函数
    let handshake = function(data) {
        data = JSON.parse(Package.strDecode(data));

        if(data.code === RES_OLD_CLIENT) {
            starx.emit('error', 'client version not fullfill');
            return;
        }

        if(data.code !== RES_OK) {
            starx.emit('error', 'handshake fail');
            return;
        }

        handshakeInit(data);

        // 如果握手没有问题 那么 回应一下服务器
        let obj = Package.encode(Package.TYPE_HANDSHAKE_ACK);
        send(obj);
        if(initCallback) {
            initCallback(socket);
        }
    };

    let heartbeatTimeoutCb = function() {

        let gap = nextHeartbeatTimeout - Date.now();
        // 如果 超时时间是大于0.1s
        if(gap > gapThreshold) {
            heartbeatTimeoutId = setTimeout(heartbeatTimeoutCb, gap);
        } else {
            //服务器回应超时  断开连接
            console.error('server heartbeat timeout');
            starx.emit('heartbeat timeout');
            starx.disconnect();
        }
    };

    //定时发送心跳包
    let heartbeat = function(data) {
        if(!heartbeatInterval) {
            // no heartbeat
            return;
        }

        let obj = Package.encode(Package.TYPE_HEARTBEAT);
        if(heartbeatTimeoutId) {
            clearTimeout(heartbeatTimeoutId);
            heartbeatTimeoutId = null;
        }

        if(heartbeatId) {
            // already in a heartbeat interval
            return;
        }
        heartbeatId = setTimeout(function() {
            heartbeatId = null;
            cc.log('发送心跳包：')
            send(obj);

            nextHeartbeatTimeout = Date.now() + heartbeatTimeout;
            heartbeatTimeoutId = setTimeout(heartbeatTimeoutCb, heartbeatTimeout);
        }, heartbeatInterval);
    };
    /**
     * 需要根据心跳包返回格式 来解析  （需要研究下js的protobuf）
     * @param data
     */
    let onData = function(data) {

        let msg = data;
        if(decode) {

            msg = decode(msg);
        }

        processMessage(starx, msg);
    };

    let onKick = function(data) {
        data = JSON.parse(Package.strDecode(data));
        starx.emit('onKick', data);
    };


    handlers[Package.TYPE_HANDSHAKE] = handshake;
    handlers[Package.TYPE_HEARTBEAT] = heartbeat;
    handlers[Package.TYPE_DATA] = onData;
    handlers[Package.TYPE_KICK] = onKick;

    let processMessage = function(starx, msg) {
        if(!msg.id) {
            // server push message
            starx.emit(msg.route, msg.body);
            return;
        }
        //if have a id then find the callback function with the request
        let cb = callbacks[msg.id];
        delete callbacks[msg.id];
        if(typeof cb !== 'function') {
            return;
        }
        cb(msg.body);

    };

    let processPackage = function(msgs) {
        if(Array.isArray(msgs)) {
            for(let i=0; i<msgs.length; i++) {
                let msg = msgs[i];
                handlers[msg.type](msg.body);
            }
        } else {
            handlers[msgs.type](msgs.body);
        }
    };


    // 封装一下发送信息函数
    let send = function(packet) {
        socket.send(packet.buffer);
    };

    // 重新初始化 重连信息
    let reset = function() {
        reconnect = false;
        reconnectionDelay = 1000 * 5;
        reconnectAttempts = 0;
        clearTimeout(reconncetTimer);
    };

    // 消息解包处理
    let deCompose = function(msg) {
        let route = msg.route;

        //Decompose route from dict
        // 压缩路由 返回 string route
        if(msg.compressRoute) {
            if(!abbrs[route]){
                return {};
            }

            route = msg.route = abbrs[route];
        }

        if(decodeIO_decoder && decodeIO_decoder.lookup(route)) {
            return decodeIO_decoder.build(route).decode(msg.body);
        } else {
            return JSON.parse(Package.strDecode(msg.body));
        }

        return msg;
    };

    //批量处理消息
    let processMessageBatch = function(starx, msgs) {
        for(let i=0, l=msgs.length; i<l; i++) {
            processMessage(starx, msgs[i]);
        }
    };

    // 收到服务器返回的 handshake 信息  设置 心跳包上报间隔 心跳包超时时间
    let handshakeInit = function(data) {
        if(data.sys && data.sys.heartbeat) {
            serializer =data.sys.serializer;
            heartbeatInterval = data.sys.heartbeat * 1000;   // heartbeat interval
            heartbeatTimeout = heartbeatInterval * 2;        // max heartbeat timeout
        } else {
            heartbeatInterval = 0;
            heartbeatTimeout = 0;
        }
        // 初始化压缩路由相关
        initData(data);
        if(typeof handshakeCallback === 'function') {
            handshakeCallback(data.user);
        }
    };


    //Initilize data used in starx client
    let initData = function(data) {
        if(!data || !data.sys) {
            return;
        }
        dict = data.sys.dict;

        //Init compress dict
        if(Object.keys(dict).length >0) {
            abbrs = {};
            for(var route in dict)
                //保存一份{code:route,...}
                abbrs[dict[route]] = route;
            }
        }

    starx.on("disconnect",function(){
        cc.log("disconnect",arguments)
    })
    window.starx = starx;
})();